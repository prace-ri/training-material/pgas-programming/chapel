Chapel
======

Compilation instructions
------------------------
In order to compile and run these examples, the only requirement is a working Chapel compiler and make. You can download Chapel at http://chapel.cray.com/.

All examples can be built with simple **make**.
